package entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTable;

/**
 *  @invariant isWellFormed()
 */

public class Bank implements BankProc {
	
	private HashMap<Person,Set<Account>> hm;
	private int nrOfPersons;
	
	public Bank(){
		hm= new HashMap<Person,Set<Account>>();	 
		readData();
	}
	
	public void removePerson(Person person) {
		assert !hm.isEmpty(); 
		int size= hm.size();
		
		hm.remove(person);
		nrOfPersons--;
		writeData();
		
		assert size-1==hm.size();
		listPersons();
	}

	public void addPerson(Person person) {
		assert person!=null;
		int size=hm.size();
		
		hm.put(person, new HashSet<Account>());
		nrOfPersons++;
		writeData();
		
		assert size+1==hm.size();
		listPersons();
	}
	
	public void editPerson(Person person, String name, String phone, String address, String mail) {
		assert person!=null;
		int size= hm.size();
		
		Person newPers = new Person(person.getId(),name,person.getCnp(),phone,address,mail);
		hm.put(newPers, hm.get(person));
		hm.remove(person);
		writeData();
		
		assert size==hm.size();
		listPersons();
	}

	public void listPersons() {
		assert !hm.isEmpty();
		
		for(Person p: hm.keySet())
	    	System.out.println(p.getId()+" " +p.getName()+" "+ p.getMail());
	}

	public void removeAccount(int id,int idHolder) {
		assert !hm.isEmpty();
		int size=hm.size();
		Account found =null;
		for(Person p: hm.keySet()){
		    	for(Set<Account> la: hm.values()){
		    		for(Account a: la){
		    			if(a.getIdAcc()== id && idHolder==p.getId())
		    				found=a;
		    		}
		    		la.remove(found);
		    	}
		    }
		
		writeData();
		
		assert size-1==hm.size();
		listAccounts();
		
	}

	public void addAccount(Person person,Account account) {
		assert person!=null;
		assert account!=null;
		
		account.addObserver(person);
		
		int size= hm.size();
		Set<Account> s= hm.get(person);
		s.add(account);
		
		hm.remove(person);
		hm.put(person,s);
		writeData();
		
		assert size+1==hm.size();
		listAccounts();
	}

	public void editAccount(int idH,int idA, int idNewH) {
		int size=hm.size();
		ArrayList<Account> accounts= getAccounts(idH);
		Account found = null;
		for(Account a: accounts)
			if(a.getIdAcc()==idA)
				found=a;
		
		removeAccount(idA,idH);
		
		found.setIdHolder(idNewH);
		Person person = getPerson(idNewH);
		addAccount(person,found);
		
		writeData();
		
		assert size==hm.size();
		listAccounts();
	}

	public void listAccounts() {
		assert !hm.isEmpty();
		for(Person p: hm.keySet()){
	    	System.out.println();
	    	for(Set<Account> la: hm.values()){
	    		for(Account a: la){
	    			if(a.getHolder()==p.getId())
	    				System.out.println("ID: "+a.getHolder()+" " +p.getName()+" Balance: "+a.getSold());
	    		}
	    	}
	    }
		
	}
	
	public Person getPerson(int id){
		assert id>=0;
		for(Person p: hm.keySet()){
			if(p.getId()==id)
				return p;
		}
		return null;
	}
	
	public ArrayList<Person> getPersons(){
		ArrayList<Person> persons= new ArrayList<Person>();
		for(Person p: hm.keySet()){
			persons.add(p);
		}
		return persons;
	}
	
	public ArrayList<Account> getAccounts(int id){
		assert id>=0;
		ArrayList<Account> accounts= new ArrayList<Account>();
		for(Person p: hm.keySet()){
			if(p.getId() == id){
				for(Set<Account> la: hm.values()){
					for(Account a: la)
						if(a.getHolder()==p.getId())
							accounts.add(a);
				}
			}
	    }
		return accounts;
	}
	
	public ArrayList<Account> getAllAccounts(){
		ArrayList<Account> accounts= new ArrayList<Account>();
	    	for(Set<Account> la: hm.values())
	    		for(Account a: la)
	    				accounts.add(a);
		return accounts;
	}
	
	public JTable createTable(ArrayList<Object> objects){
		Object o= objects.get(0);
		Object[] headers;
		if(o instanceof Person)
			headers= new Object[o.getClass().getDeclaredFields().length];
		else
			headers= new Object[o.getClass().getSuperclass().getDeclaredFields().length];
		int i =0;
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			try {
				headers[i]=field.getName();
				i++;

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	
		Object[][] data= new Object[objects.size()][headers.length];
		
		for(int j=0;j<objects.size();j++){
			int k=0;
			for (Field field : objects.get(j).getClass().getDeclaredFields()) {
				field.setAccessible(true); // set modifier to public
				try {
					data[j][k] = field.get(objects.get(j));
					k++;

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (ArrayIndexOutOfBoundsException e){
					e.printStackTrace();
				}
			}
		}
		
		JTable table= new JTable(data,headers);
		
		return table;
	}
	
	public boolean isWellFormed(){
		return nrOfPersons==hm.size();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void readData() {
		 try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         
	         hm = (HashMap) in.readObject();
	         in.close();
	         fileIn.close();
	         
		 }catch(IOException i) {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c) {
	         System.out.println("Person class not found");
	         c.printStackTrace();
	         return;
	      }
	}

	
	public void writeData() {
		try{
			
		FileOutputStream fileOut = new FileOutputStream("bank.ser");
	    ObjectOutputStream out = new ObjectOutputStream(fileOut);

	    out.writeObject(hm);
	    out.close();
	    fileOut.close();
	 
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
