package entities;

import java.io.Serializable;
import java.util.Observable;

@SuppressWarnings("serial")
public abstract class Account extends Observable implements Serializable {
	
	private int idAcc;  
	private int idHolder;
	private int balance;
	
	public Account(int idAcc, int idHolder, int balance){
		this.idAcc=idAcc;
		this.idHolder=idHolder;
		this.balance=balance;
	}
	
	public abstract void deposit(Integer sum);
	
	public abstract void withdraw(Integer sum);
	
	public int getHolder(){
		return idHolder;
	}
	
	public void setIdHolder(int idHolder) {
		this.idHolder = idHolder;
	}
	
	public int getIdAcc() {
		return idAcc;
	}
	
	public int getSold() {
		return balance;
	}

}
