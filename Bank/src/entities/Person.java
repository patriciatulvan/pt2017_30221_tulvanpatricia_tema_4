package entities;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class Person implements Observer,Serializable {
	
	private int id;
	private String name;
	private String cnp;
	private String phone;
	private String address;
	private String mail;
	
	public Person(Integer id, String name,String cnp, String phone, String address, String mail){
		this.id=id;
		this.name=name;
		this.cnp=cnp;
		this.phone=phone;
		this.address=address;
		this.mail=mail;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getCnp() {
		return cnp;
	}
	
	
	@Override
	public int hashCode(){
		//System.out.println("hash:"+ Integer.parseInt(cnp)%907%1000);
		return Integer.parseInt(cnp)%907%100;
	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("Am ajuns in update!");
		List<String> lines= Arrays.asList(" ","Your account has suffered changes."," ","Account number:"+ ((Account)o).getIdAcc(),"Account's holder ID: " +((Account)o).getHolder() ,"Initial balace: "+(Integer)arg,"Current balance: "+((Account)o).getSold());
		Path file= Paths.get("file"+((Account)o).getIdAcc()+".txt");
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("Can't print!");
		}
		
	}
		
}
