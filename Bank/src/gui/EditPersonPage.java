package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entities.Bank;

public class EditPersonPage extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public EditPersonPage(){
		setTitle("Edit Person");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 600, 300);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn = new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelLbl = new JPanel();
		panelLbl.setBackground(Color.LIGHT_GRAY);
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		
		JPanel panelText = new JPanel();
		panelText.setBackground(Color.LIGHT_GRAY);
		panelText.setLayout(new BoxLayout(panelText,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg = new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		panelImg.setLayout(new BoxLayout(panelImg,BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		JLabel img;
		ImageIcon e= new ImageIcon("edit.png");
		img= new JLabel(e);
		panelImg.add(img);
		
		JLabel giveId = new JLabel();
		giveId.setText("Choose the person's ID: ");
		giveId.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel nameLbl = new JLabel();
		nameLbl.setText("Name: ");
		nameLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel addressLbl = new JLabel();
		addressLbl.setText("Address: ");
		addressLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel phoneLbl = new JLabel();
		phoneLbl.setText("Phone number: ");
		phoneLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel mailLbl= new JLabel();
		mailLbl.setText("Mail: ");
		mailLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		panelLbl.add(giveId);
		panelLbl.add(nameLbl);
		panelLbl.add(phoneLbl);
		panelLbl.add(addressLbl);
		panelLbl.add(mailLbl);
		
		
		Bank bank= FirstPage.bank;
		Integer[] idHolders= new Integer[bank.getPersons().size()];
		for(int i=0;i<bank.getPersons().size();i++)
			idHolders[i]=bank.getPersons().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);
		idCombo.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		
		JTextField name= new JTextField();
		name.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		name.setColumns(30);
		
		JTextField address= new JTextField();
		address.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		address.setColumns(30);
		
		JTextField phone= new JTextField();
		phone.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		phone.setColumns(30);
		
		JTextField mail= new JTextField();
		mail.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		mail.setColumns(30);
		
		panelText.add(idCombo);
		panelText.add(name);
		panelText.add(phone);
		panelText.add(address);
		panelText.add(mail);		
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new PersonOpPage();
				setVisible(false);
				dispose();
			}
		});
		
		JButton edit = new JButton();
		edit.setText("Edit Person");
		edit.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0 && !name.getText().equals("") && !phone.getText().equals("") && !address.getText().equals("") && !mail.getText().equals("")){
					int id= (Integer)idCombo.getSelectedItem();
					bank.editPerson(bank.getPerson(id), name.getText(), phone.getText(), address.getText(), mail.getText());
					JOptionPane.showMessageDialog(null, "Successful Operation! ", "Edit Person", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Edit Person", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		JButton reset = new JButton();
		reset.setText("Reset");
		reset.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				name.setText("");
				address.setText("");
				phone.setText("");
				mail.setText("");
			}
		});
		panelBtn.add(back);
		panelBtn.add(reset);
		panelBtn.add(edit);
		
		bigPanel.add(panelLbl);
		bigPanel.add(panelText);
		bigPanel.add(panelBtn);
		panel.add(panelImg);
		panel.add(bigPanel);
		
		setContentPane(panel);
		setVisible(true);
	}
	
}
