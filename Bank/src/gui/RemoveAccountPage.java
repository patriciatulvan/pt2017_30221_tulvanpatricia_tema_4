package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.Bank;

public class RemoveAccountPage extends JFrame{
	private static final long serialVersionUID = 1L;
	private int idH;
	private JComboBox<Integer> idA;

	public RemoveAccountPage(){
		setTitle("Remove Account");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 500, 250);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg= new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelD= new JPanel();
		panelD.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn= new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon del= new ImageIcon("delete.png");
		img= new JLabel(del);
		panelImg.add(img);
		
		JLabel giveId = new JLabel();
		giveId.setText("Choose the account's ID: ");
		giveId.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel giveHolder= new JLabel();
		giveHolder.setText("Choose the account's holder ID");
		giveHolder.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		
		Bank bank= FirstPage.bank;
		Integer[] idHolders= new Integer[bank.getPersons().size()];
		for(int i=0;i<bank.getPersons().size();i++)
			idHolders[i]=bank.getPersons().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);
		idCombo.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		panelD.add(idCombo);
			
		panel.add(giveHolder);
		panel.add(idCombo);
		
		JButton back= new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountOpPage();
				setVisible(false);
				dispose();
			}
		});
		panelBtn.add(back);
		
		
		JButton setH= new JButton();
		setH.setText("Set holder");
		setH.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		setH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0 ){
					Integer idHolder=(Integer)idCombo.getSelectedItem();
					System.out.println(idHolder);
					Integer[] id = new Integer[bank.getAccounts(idHolder).size()];
					for(int i=0;i<bank.getAccounts(idHolder).size();i++)
						id[i]=bank.getAccounts(idHolder).get(i).getIdAcc();
					JComboBox<Integer> idAcc= new JComboBox<>(id);
					idAcc.setFont(new Font("Book Antiqua", Font.BOLD, 13));
				
					panelD.removeAll();
					panelD.add(giveId);
					panelD.add(idAcc);
				
					saveHolder(idHolder);
					saveCombo(idAcc);
				
					setContentPane(bigPanel);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Remove Account", JOptionPane.ERROR_MESSAGE);
			}
		});
		panelBtn.add(setH);
		
		JButton delete = new JButton();
		delete.setText("Remove Account");
		delete.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getAccounts(idH).size()>0){
					bank.removeAccount((Integer)idA.getSelectedItem(), idH);
					JOptionPane.showMessageDialog(null, "Successful Operation! ", "Remove Account", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Remove Account", JOptionPane.ERROR_MESSAGE);
			}
		});
		panelBtn.add(delete);
		
		bigPanel.add(panelImg);
		bigPanel.add(panel);
		bigPanel.add(panelD);
		bigPanel.add(panelBtn);
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	private void saveHolder(int idHolder){
		idH=idHolder;
	}
	
	private void saveCombo(JComboBox<Integer> combo){
		idA=combo;
	}
}
