package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entities.Account;
import entities.Bank;
import entities.Person;
import entities.SavingAccount;
import entities.SpendingAccount;

public class AddAccountPage extends JFrame {
	private static final long serialVersionUID = 1L;

	public AddAccountPage(){
		setTitle("Add Account");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 600, 250);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn = new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelLbl = new JPanel();
		panelLbl.setBackground(Color.LIGHT_GRAY);
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		
		JPanel panelText = new JPanel();
		panelText.setBackground(Color.LIGHT_GRAY);
		panelText.setLayout(new BoxLayout(panelText,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg = new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		panelImg.setLayout(new BoxLayout(panelImg,BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		
		JLabel img;
		ImageIcon ad= new ImageIcon("add.png");
		img= new JLabel(ad);
		panelImg.add(img);
		
		JLabel idLbl = new JLabel();
		idLbl.setText("ID Account: ");
		idLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel idHolderLbl= new JLabel();
		idHolderLbl.setText("ID Holder: ");
		idHolderLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel balanceLbl = new JLabel();
		balanceLbl.setText("Balance: ");
		balanceLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel typeLbl = new JLabel();
		typeLbl.setText("Type: ");
		typeLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		panelLbl.add(idLbl);
		panelLbl.add(idHolderLbl);
		panelLbl.add(balanceLbl);
		panelLbl.add(typeLbl);
		
		JTextField idAcc = new JTextField();
		idAcc.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		idAcc.setColumns(30);
		
		JTextField idHolder= new JTextField();
		idHolder.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		idHolder.setColumns(30);
		
		JTextField balance = new JTextField();
		balance.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		balance.setColumns(30);
		
		panelText.add(idAcc);
		panelText.add(idHolder);
		panelText.add(balance);
		
		String[] type = new String[2];
		type[0]="Saving Account";
		type[1]="Spending Account";
		JComboBox<String> idCombo= new JComboBox<>(type);
		idCombo.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		panelText.add(idCombo);
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountOpPage();
				setVisible(false);
				dispose();
			}
		});
		
		JButton add = new JButton();
		add.setText("Add Account");
		add.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!idAcc.getText().equals("") && !idHolder.getText().equals("") && !balance.getText().equals("")){
					int id= Integer.parseInt(idAcc.getText());
					int idH=Integer.parseInt(idHolder.getText());
					Integer bl= Integer.parseInt(balance.getText());
					String type= (String)idCombo.getSelectedItem();
					if(type.equals("Saving Account")){
						Bank bank= FirstPage.bank;
						Person person= bank.getPerson(idH);
						if(person!=null){
							Account account= new SavingAccount(id,idH,bl);
							bank.addAccount(person, account);
							JOptionPane.showMessageDialog(null, "Account Added! ", "Add Account", JOptionPane.INFORMATION_MESSAGE);
						}
						else
							JOptionPane.showMessageDialog(null, "Failed Operation! No person with this ID! ", "Add Account", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						Bank bank= FirstPage.bank;
						Person person= bank.getPerson(idH);
						Account account= new SpendingAccount(id,idH,bl);
						bank.addAccount(person, account);
						JOptionPane.showMessageDialog(null, "Account Added! ", "Add Account", JOptionPane.INFORMATION_MESSAGE);
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Add Account", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		JButton reset = new JButton();
		reset.setText("Reset");
		reset.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				idAcc.setText("");
				idHolder.setText("");
				balance.setText("");
			}
		});
		panelBtn.add(back);
		panelBtn.add(reset);
		panelBtn.add(add);
		
		bigPanel.add(panelLbl);
		bigPanel.add(panelText);
		bigPanel.add(panelBtn);
		
		panel.add(panelImg);
		panel.add(bigPanel);
		
		setContentPane(panel);
		setVisible(true);
	}
}
