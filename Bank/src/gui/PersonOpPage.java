package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PersonOpPage extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public PersonOpPage(){
		setTitle("Person Operations");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 600,200);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel personPanel = new JPanel();
		personPanel.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon folder= new ImageIcon("folder.png");
		img= new JLabel(folder);
		bigPanel.add(img);
		
		JButton addC = new JButton();
		addC.setText("Add Person");
		addC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		addC.setBackground(new Color(255,204,229));
		addC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AddPersonPage();
				setVisible(false);
				dispose();
			}
		});
		personPanel.add(addC);
		
		JButton deleteC = new JButton();
		deleteC.setText("Remove Person");
		deleteC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		deleteC.setBackground(new Color(255,204,229));
		deleteC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RemovePersonPage();
				setVisible(false);
				dispose();
			}
		});
		personPanel.add(deleteC);
		
		JButton editC = new JButton();
		editC.setText("Edit Person");
		editC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		editC.setBackground(new Color(255,204,229));
		editC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new EditPersonPage();
				setVisible(false);
				dispose();
			}
		});
		personPanel.add(editC);
		
		JButton viewC = new JButton();
		viewC.setText("List Persons");
		viewC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		viewC.setBackground(new Color(255,204,229));
		viewC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ListPersonsPage();
				setVisible(false);
				dispose();
			}
		});
		personPanel.add(viewC);
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.setBackground(new Color(153,204,255));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		personPanel.add(back);
		
		bigPanel.add(personPanel);
		setContentPane(bigPanel);
		setVisible(true);
	}
}
